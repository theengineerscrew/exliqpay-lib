defmodule ExLiqpay do
  @moduledoc """
  LiqPay Client

  More informations on https://www.liqpay.ua/documentation/api/home/
  """

  alias ExLiqpay.Client
  require OK

  @version 3
  @public_key Application.get_env(:exliqpay, :public_key, "")
  @private_key Application.get_env(:exliqpay, :private_key, "")
  @request "https://www.liqpay.ua/api/request"
  @checkout "https://www.liqpay.ua/api/3/checkout"


  @doc """
  Create pay and return link

  About parameters - https://www.liqpay.ua/documentation/api/aquiring/checkout/doc
  """
  @spec pay(Map.t) :: {:ok, String.t} | {:error, HTTPoison.Error.t}
  def pay(params) do
    params
    |> Map.merge(%{action: "pay"})
    |> send_off(@checkout)
    |> link()
  end

  @doc """
  Create data and signature

  About parameters - https://www.liqpay.ua/documentation/api/aquiring/checkout/doc
  """
  @spec pay(Map.t) :: {:ok, String.t} | {:error, HTTPoison.Error.t}
  def pay_widget(params) do
    params
    |> Map.merge(%{action: "pay"})
    |> send_off()
  end

  @doc """
  Create subscription and return link

  About parameters - https://www.liqpay.ua/documentation/api/aquiring/checkout/doc
  """
  @spec subscription(Map.t) :: {:ok, String.t} | {:error, HTTPoison.Error.t}
  def subscription(params) do
    params
    |> Map.merge(%{action: "subscribe", subscribe: "1"})
    |> send_off(@checkout)
    |> link()
  end

  @doc """
  Check LiqPay callback
  """
  @spec callback(Map.t) :: {:ok, Map.t} | {:error, String.t}
  def callback(%{"data" => d, "signature" => s}) do
    :sha
    |> :crypto.hash(@private_key <> d <> @private_key)
    |> :base64.encode()
    |> Kernel.==(s)
    |> case do
      true -> Jason.decode(:base64.decode(d))
      false -> OK.failure("not valid")
    end
  end

  @doc """
  Cancel subscription
  """
  @spec cancel_subscription(String.t) :: {:ok, String.t} | {:error, String.t}
  def cancel_subscription(order_id) do
    %{action: "unsubscribe",order_id: order_id}
    |> send_off(@request)
    |> body()
  end

  @doc """
  Check Liqpay payment status
  """
  @spec status(String.t) :: {:ok, String.t} | {:error, String.t}
  def status(order_id) do
    %{action: "status", order_id: order_id}
    |> send_off(@request)
    |> body()
  end

  @doc """
  Send ticket result payment on email
  """
  @spec ticket(String.t, String.t) :: {:ok, String.t} | {:error, String.t}
  def ticket(email, order_id) do
    %{action: "ticket", email: email, order_id: order_id}
    |> send_off(@request)
    |> body()
  end

  @doc false
  defp link(response) do
    response
    |> OK.flat_map(fn resp ->
      resp
      |> Map.get(:headers)
      |> Enum.into(%{})
      |> Map.get("Location")
      |> OK.required("not found Location")
    end)
  end

  @doc false
  defp body(response) do
    response
    |> OK.flat_map(fn resp -> Map.get(resp, :body) end)
    |> OK.required("not found body response")
    |> OK.flat_map(fn body -> Jason.decode(body) end)
  end

  @doc false
  defp send_off(params, url) do
    attrs = Map.merge(params, %{
      version: @version,
      public_key: @public_key
    })

    data = data(attrs)
    signature = signature(data)

    Client.post(url, %{data: data, signature: signature})
  end

  @doc false
  defp send_off(params) do
    attrs = Map.merge(params, %{
      version: @version,
      public_key: @public_key
    })

    data = data(attrs)
    signature = signature(data)

    %{data: data, signature: signature}
  end

  @doc false
  defp data(params) do
    params
    |> Jason.encode()
    |> OK.flat_map(fn p -> :base64.encode(p) end)
  end

  @doc false
  defp signature(data) do
    :sha
    |> :crypto.hash(@private_key <> data <> @private_key)
    |> :base64.encode()
  end
end
