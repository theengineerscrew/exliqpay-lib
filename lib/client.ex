defmodule ExLiqpay.Client do
  @moduledoc false
  use HTTPoison.Base

  @doc false 
  def process_url(url), do: url
  
  @doc false
  def process_request_body(%{data: d, signature: s}) do
    {:form, [{"data", d}, {"signature", s}]}
  end
end