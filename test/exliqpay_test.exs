defmodule ExLiqpayTest do
  use ExUnit.Case
  doctest ExLiqpay

  test "pay" do
    params = %{
      amount: 123,
      description: "test",
      currency: "RUB"
    }

    assert {:ok, "https://www.liqpay.ua/en/checkout/" <> _} = ExLiqpay.pay(params)
  end

  test "subscription" do
    params = %{
      amount: 123,
      description: "test",
      currency: "RUB",
      subscribe_date_start: "2015-03-31 00:00:00",
      subscribe_periodicity: "month"
    }

    assert {:ok, "https://www.liqpay.ua/en/checkout/" <> _} = ExLiqpay.subscription(params)
  end

  test "cancel_subscription" do
    assert {:ok, %{"action" => "unsubscribe"}} = ExLiqpay.cancel_subscription("123")
  end

  test "status" do
    assert {:ok, %{}} = ExLiqpay.status("123")
  end

  test "ticket" do
    assert {:ok, %{}} = ExLiqpay.ticket("example@gmail.com", "123")
  end
end
