# ExLiqpay

Liqpay Client on Elixir

## Documentation

API documentation is available at [https://hexdocs.pm/exliqpay](https://hexdocs.pm/exliqpay)

## Installation

mix.exs

```elixir
defp deps do
  [{:exliqpay, "..."}]
end
```

Add your configuration

```elixir
config :my_app, ExLiqpay,
  public_key: "my_public_key",
  private_key: "my_private_key"
```

## Basics

```elixir
# create pay link
{:ok, link} = ExLiqpay.pay(%{})

# create subscription link
{:ok, link} = ExLiqpay.subscription(%{})

# cancel subscription
{:ok, link} = ExLiqpay.cancel_subscription("order_id")

# check status payment
{:ok, result} = ExLiqpay.status("order_id")

# send on email ticket payment
{:ok, result} = ExLiqpay.ticket("example@gmail.com", "order_id")

# check LiqPay callback
{:ok, result} = ExLiqpay.callback(%{"data" => "...", "signature" => "..."})
```