defmodule ExLiqpay.MixProject do
  use Mix.Project

  def project do
    [
      app: :exliqpay,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "ExLiqpay",
      # source_url: "https://github.com/USER/PROJECT",
      # homepage_url: "http://YOUR_PROJECT_HOMEPAGE",
      # docs: [main: "ExLiqpay", # The main page in the docs
              # logo: "path/to/logo.png",
              # extras: ["README.md"]]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.1.2"},
      {:ok, "~> 2.0.0"},
      {:httpoison, "~> 1.1"},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false}
    ]
  end
end
